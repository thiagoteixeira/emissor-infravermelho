#include <IRremote.h>


IRsend irsend;
int ledEmissorInfravermelho = 5;
int led = 13;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledEmissorInfravermelho, OUTPUT);
  pinMode(led, OUTPUT);
  Serial.begin(9600); 
}

void loop() {

  //0xE17AB04F codigo aumentar volume - apenas para testar emissao
  irsend.sendNEC(0xE17AB04F, 32);
  digitalWrite(led, HIGH);
  delay(3000);
  digitalWrite(led, LOW);
  delay(1000);

  //0xE17AB04F codigo abaixar volume - apenas para testar emissao
  irsend.sendNEC(0xE17A708F, 32);
  digitalWrite(led, HIGH);  
  delay(3000);
  digitalWrite(led, LOW);
  delay(1000);
}
